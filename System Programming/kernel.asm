; ** por compatibilidad se omiten tildes **
; ==============================================================================
; TALLER System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
; ==============================================================================

%include "print.mac"


global start


; COMPLETAR - Agreguen declaraciones extern según vayan necesitando
;extern gdt
extern GDT_DESC
extern IDT_DESC
extern pic_reset
extern pic_enable
extern screen_draw_layout
extern idt_init
extern mmu_init_kernel_dir
extern copy_page
extern mmu_init
extern mmu_map_page
extern mmu_init_task_dir
extern tss_init
extern sched_init
extern task_init
; COMPLETAR - Definan correctamente estas constantes cuando las necesiten
%define CS_RING_0_SEL (1 << 3)
%define DS_RING_0_SEL (3 << 3)
%define SELECTOR_TAREA_INIT (11 << 3)
%define SELECTOR_TAREA_IDLE (12 << 3)


BITS 16
;; Saltear seccion de datos
jmp start

;;
;; Seccion de datos.
;; -------------------------------------------------------------------------- ;;
start_rm_msg db     'Iniciando kernel en Modo Real'
start_rm_len equ    $ - start_rm_msg

start_pm_msg db     'Iniciando kernel en Modo Protegido'
start_pm_len equ    $ - start_pm_msg

;;
;; Seccion de código.
;; -------------------------------------------------------------------------- ;;

;; Punto de entrada del kernel.
BITS 16
start:
    ; COMPLETAR - Deshabilitar interrupciones
    
    cli
    
    ; Cambiar modo de video a 80 X 50
    mov ax, 0003h
    int 10h ; set mode 03h
    xor bx, bx
    mov ax, 1112h
    int 10h ; load 8x8 font

    ; COMPLETAR - Imprimir mensaje de bienvenida - MODO REAL
    ; (revisar las funciones definidas en print.mac y los mensajes se encuentran en la
    ; sección de datos)

    print_text_rm start_rm_msg, start_rm_len, 6, 0, 0

    ; COMPLETAR - Habilitar A20
    ; (revisar las funciones definidas en a20.asm)
    call A20_enable
    
    ; COMPLETAR - Cargar la GDT
    lgdt [GDT_DESC]

    ;xchg bx, bx
    ; COMPLETAR - Setear el bit PE del registro CR0
    mov eax, cr0
    or eax, 1
    mov cr0, eax

    ; COMPLETAR - Saltar a modo protegido (far jump)
    ; (recuerden que un far jmp se especifica como jmp CS_selector:address)
    ; Pueden usar la constante CS_RING_0_SEL definida en este archivo
    jmp CS_RING_0_SEL:modo_protegido
    
BITS 32
modo_protegido:
    ; COMPLETAR - A partir de aca, todo el codigo se va a ejectutar en modo protegido
    ; Establecer selectores de segmentos DS, ES, GS, FS y SS en el segmento de datos de nivel 0
    ; Pueden usar la constante DS_RING_0_SEL definida en este archivo
    
    mov bx, DS_RING_0_SEL
    mov ds, bx
    mov es, bx
    mov gs, bx
    mov fs, bx
    mov ss, bx

    ; COMPLETAR - Establecer el tope y la base de la pila
    mov esp, 0x25000
    mov ebp, esp

    ; COMPLETAR - Imprimir mensaje de bienvenida - MODO PROTEGIDO
    print_text_pm start_pm_msg, start_pm_len, 6, 2, 0
    ;xchg bx, bx
    ; COMPLETAR - Inicializar pantalla
    call screen_draw_layout
    
    ;cargamos el idtr la direccion de la idt
    call idt_init
    lidt [IDT_DESC]
    
    ;seteamos el PIC
    call pic_reset
    call pic_enable

    call mmu_init_kernel_dir

    mov cr3, eax

    mov eax, cr0
    or  eax, 0x80000000
    mov cr0, eax

    call tss_init

    call sched_init



    mov ax, SELECTOR_TAREA_INIT
    ;xchg bx,bx
    ltr ax
    xchg bx,bx
    call task_init
    xchg bx,bx
    sti    
    
    jmp SELECTOR_TAREA_IDLE:0

    xchg bx,bx



    ; Ciclar infinitamente 
    mov eax, 0xFFFF
    mov ebx, 0xFFFF
    mov ecx, 0xFFFF
    mov edx, 0xFFFF
    jmp $

;; -------------------------------------------------------------------------- ;;

%include "a20.asm"
