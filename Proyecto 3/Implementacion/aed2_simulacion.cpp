#include "aed2_simulacion.h"
#include "aed2_mapa.h"

aed2_Simulacion::aed2_Simulacion(const aed2_Mapa &mapa, const map<Color, Coordenada> &objetos, Coordenada inicio)
        : misimulacion_(mapa.mapa(), objetos, inicio),objetosAed2_(objetos) {

}
void aed2_Simulacion::simulacionToAed2Simulacion(){
    for(Color x : misimulacion_.coloresObjetos()){
        objetosAed2_[x]=misimulacion_.posObjetos().at(x);
    }
}

void aed2_Simulacion::mover(Direccion d) {

    misimulacion_.mover(d);
    simulacionToAed2Simulacion();
}

void aed2_Simulacion::agregarObjetivo(Objetivo o) {
    misimulacion_.agregarObjetivo(o);
}

const aed2_Mapa aed2_Simulacion::mapa() const {
    return aed2_Mapa(misimulacion_.mapa());
}

const map<Color, Coordenada> &aed2_Simulacion::posObjetos() const {
    return objetosAed2_;
}

Coordenada aed2_Simulacion::posAgente() const {
    return misimulacion_.posAgente();
}

const list<Objetivo> &aed2_Simulacion::objetivosDisponibles() const {

    return misimulacion_.objetivosDisponibles();
}

Nat aed2_Simulacion::movimientos() const {
    return misimulacion_.movimientos();
}

Nat aed2_Simulacion::objetivosResueltos() const {
    return misimulacion_.objetivosResueltos();
}
