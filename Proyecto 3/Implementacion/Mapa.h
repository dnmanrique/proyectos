#ifndef MAPA_H
#define MAPA_H

#include "Tipos.h"
#include <string>
#include "Objetivo.h"

using namespace std;


class Mapa {
public:

    /** Pre: Todas las elevaciones están en rango y todas las coordenadas de receptáculos están en rango y no se repiten */

    Mapa();

    Mapa(Nat ancho, Nat alto, map<Color,Coordenada> receptaculos);

    void agregarRampa(Coordenada c);

    void agregarPared(Coordenada c);

    bool enRango(Coordenada c);

    Nat ancho() const;

    Nat alto() const;

    TipoCasillero tipoCasillero(Coordenada) const;

    const string_map<Coordenada> &receptaculos() const;

    Coordenada receptaculos(Color col);

    Nat distancia(Coordenada c1, Coordenada c2);

    vector<vector<Color>> ubicReceptaculos();

    const set<Color> &coloresReceptaculos() const;



private:

    void armarMapaReceptaculos(vector<vector<Color>> &mapRec, map<Color,Coordenada> map);

    void armarMapa(vector<vector<TipoCasillero>> &mapEle, TipoCasillero);

    list<Coordenada> posicionesAdyacentes(Coordenada c);

    bool existePared(list<Coordenada> lp);

    bool existePiso(list<Coordenada> lp);

    list<Coordenada> rampasAdyacentes(Coordenada c);

    Coordenada suma(Coordenada c1, Coordenada c2);

    Nat ancho_;

    Nat alto_;

    string_map<Coordenada> receptaculos_;

    vector<vector<TipoCasillero>> ubicacionElevaciones_;

    vector<vector<Color>> ubicacionReceptaculos_;

    set<Color> coloresEnReceptaculos_;

    Coordenada resta(Coordenada c1, Coordenada c2);



};


#endif 
