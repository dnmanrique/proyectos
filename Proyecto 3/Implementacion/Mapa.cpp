#include "Mapa.h"

Mapa::Mapa() : alto_(0), ancho_(0), receptaculos_(string_map<Coordenada>()) {}

Mapa::Mapa(Nat ancho, Nat alto, std::map<Color, Coordenada> receptaculos) {
    alto_ = alto;
    ancho_ = ancho;

    for (pair<Color, Coordenada> x: receptaculos) {
        receptaculos_.insert(make_pair(x.first, x.second));
        coloresEnReceptaculos_.insert(x.first);
    }

    vector<vector<TipoCasillero>> elevacionesAux(ancho);
    vector<TipoCasillero> filas(alto);
    for (Nat i = 0; i < ancho; i++) {
        elevacionesAux[i] = filas;
    }
    ubicacionElevaciones_ = elevacionesAux;
    armarMapa(ubicacionElevaciones_, PISO);
    vector<vector<Color>> recepAux(ancho);
    vector<Color> Colorfilas(alto);
    for (Nat j = 0; j < ancho; j++) {
        recepAux[j] = Colorfilas;
    }
    ubicacionReceptaculos_ = recepAux;
    armarMapaReceptaculos(ubicacionReceptaculos_, receptaculos);
}

void Mapa::armarMapa(vector<vector<TipoCasillero>> &mapEle, TipoCasillero e) {
    for (int i = 0; i < mapEle.size(); i++) {
        for (int j = 0; j < mapEle[0].size(); j++) {
            mapEle[i][j] = e;
        }
    }
}

void Mapa::armarMapaReceptaculos(vector<vector<Color>> &map, std::map<Color, Coordenada> rs) {
    for (int i = 0; i < map.size(); i++) {
        for (int j = 0; j < map[0].size(); j++) {
            map[i][j] = "";     //modificamos " " por "";
        }
    }
    auto it1 = rs.begin();
    while (it1 != rs.end()) {
        pair<Nat, Nat> posicion = make_pair(it1->second.first, it1->second.second);
        map[posicion.first][posicion.second] = it1->first;
        ++it1;
    }
}

void Mapa::agregarPared(Coordenada c) {
    //if (enRango(c) && tipoCasillero(c) == PISO && existePiso(rampasAdyacentes(c))) {
    ubicacionElevaciones_[c.first][c.second] = ELEVACION;
    // }
}


void Mapa::agregarRampa(Coordenada c) {
    //if (enRango(c) && tipoCasillero(c) == PISO && existePared(posicionesAdyacentes(c)) && existePiso(posicionesAdyacentes(c)) && existePiso(rampasAdyacentes(c))) {
    ubicacionElevaciones_[c.first][c.second] = RAMPA;
    //}
}

list<Coordenada> Mapa::rampasAdyacentes(Coordenada c) {
    list<Coordenada> res = list<Coordenada>();
    if (enRango(suma(c, make_pair(1, 0))) && tipoCasillero(suma(c, make_pair(1, 0))) == RAMPA) {
        res.push_back(suma(c, make_pair(1, 0)));
    }
    if (enRango(resta(c, make_pair(1, 0))) && tipoCasillero(resta(c, make_pair(1, 0))) == RAMPA) {      //resta
        res.push_back(resta(c, make_pair(1, 0)));
    }
    if (enRango(suma(c, make_pair(0, 1))) && tipoCasillero(suma(c, make_pair(0, 1))) == RAMPA) {
        res.push_back(suma(c, make_pair(0, 1)));
    }
    if (enRango(resta(c, make_pair(0, 1))) && tipoCasillero(resta(c, make_pair(0, 1))) == RAMPA) {
        res.push_back(resta(c, make_pair(0, 1)));
    }
    return res;
}

Coordenada Mapa::suma(Coordenada c1, Coordenada c2) {
    Coordenada res = make_pair(0, 0);
    res.first = c1.first + c2.first;
    res.second = c1.second + c2.second;
    return res;
}

Coordenada Mapa::resta(Coordenada c1, Coordenada c2) {
    Coordenada res = make_pair(0, 0);
    if (c1.first == 0 && c2.first > 0) {
        res.first = this->alto_ + 1; //se va de rango
    }
    res.first = c1.first + c2.first;
    res.second = c1.second + c2.second;
    return res;
}

list<Coordenada> Mapa::posicionesAdyacentes(Coordenada c) {
    list<Coordenada> res = list<Coordenada>();
    if (enRango(suma(c, make_pair(1, 0)))) {
        res.push_back(suma(c, make_pair(1, 0)));
    }
    if (enRango(suma(c, make_pair(-1, 0)))) {
        res.push_back(suma(c, make_pair(-1, 0)));
    }
    if (enRango(suma(c, make_pair(0, 1)))) {
        res.push_back(suma(c, make_pair(0, 1)));
    }
    if (enRango(suma(c, make_pair(0, -1)))) {
        res.push_back(suma(c, make_pair(0, -1)));
    }
    return res;
}

bool Mapa::existePiso(list<Coordenada> lp) {
    auto itLista = lp.begin();
    while (itLista != lp.end()) {
        if (tipoCasillero(*itLista) == PISO) {
            return true;
        }
        ++itLista;
    }
    return false;
}

bool Mapa::existePared(list<Coordenada> lp) {
    auto itLista = lp.begin();
    while (itLista != lp.end()) {
        if (tipoCasillero(*itLista) == ELEVACION) {
            return true;
        }
        ++itLista;
    }
    return false;
}



bool Mapa::enRango(Coordenada c) {
    bool res = 0 <= c.first && c.first < ancho_ && 0 <= c.second && c.second < alto_;
    return res;
}

Nat Mapa::ancho() const {
    return ancho_;
}

Nat Mapa::alto() const {
    return alto_;
}

TipoCasillero Mapa::tipoCasillero(Coordenada c) const {
    TipoCasillero res;
    res = ubicacionElevaciones_[c.first][c.second];
    return res;
}

const string_map<Coordenada> &Mapa::receptaculos() const {
    return receptaculos_;
};

Coordenada Mapa::receptaculos(Color col) {
    return receptaculos_.at(col);

}

Nat Mapa::distancia(Coordenada c1, Coordenada c2) {
    int DistanciaX = c1.first - c2.first;
    int DistanciaY = c1.second - c2.second;
    if (DistanciaX < 0) {
        DistanciaX = DistanciaX * (-1);
    }
    if (DistanciaY < 0) {
        DistanciaY = DistanciaY * (-1);
    }
    return DistanciaX + DistanciaY;
}

vector<vector<Color>> Mapa::ubicReceptaculos() {
    return ubicacionReceptaculos_;
}

const set<Color> &Mapa::coloresReceptaculos() const {
    return coloresEnReceptaculos_;
}