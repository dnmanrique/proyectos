#include "aed2_mapa.h"

aed2_Mapa::aed2_Mapa() : mimapa_(),recept_(map<Color ,Coordenada>()) {}

aed2_Mapa::aed2_Mapa(Nat ancho, Nat alto, set<Coordenada> elevaciones, map<Color, Coordenada> receptaculos) : mimapa_(
        ancho, alto, receptaculos),recept_(receptaculos) {
    /** Aca agrego el conjunto de elevaciones que son las paredes del mapa  */
    for (Coordenada p: elevaciones) {
        mimapa_.agregarPared(p);
    }

}

void aed2_Mapa::recepToAed2Recep() {
    for (Color x: mimapa_.coloresReceptaculos()) {
        recept_[x]=mimapa_.receptaculos().at(x);

    }
}

void aed2_Mapa::agregarRampa(Coordenada c) {
    mimapa_.agregarRampa(c);

}

Nat aed2_Mapa::ancho() const {
    return mimapa_.ancho();
}

Nat aed2_Mapa::alto() const {
    return mimapa_.alto();
}



TipoCasillero aed2_Mapa::tipoCasillero(Coordenada c) const {
    return mimapa_.tipoCasillero(c);
}

Coordenada aed2_Mapa::receptaculo(Color col) {
    return mimapa_.receptaculos(col);
}

const map<Color, Coordenada> &aed2_Mapa::receptaculos() const {

    return recept_;
}

Mapa aed2_Mapa::mapa() const {
    return mimapa_;
}

aed2_Mapa::aed2_Mapa(Mapa m) : mimapa_(m) {recepToAed2Recep();}






//completar