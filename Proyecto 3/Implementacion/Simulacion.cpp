#include "Simulacion.h"

Simulacion::Simulacion(const Mapa &mapa, const map<Color, Coordenada> &objetos, Coordenada inicio) {
    mapa_ = mapa;
    objetivosDisponibles_ = list<Objetivo>();
    objetivosActuales_ = string_map<string_map<list<Objetivo>::iterator >>(); //Aca declaramos objetivosActuales
    cantMovimientos_ = 0;
    objetivosCompletados_ = 0;
    posicionAgente_ = inicio;
    for (pair<Color, Coordenada> x: objetos) {
        objetos_.insert(make_pair(x.first, x.second));
        coloresObjetos_.insert(x.first);
    }
    vector<vector<Color>> matriz;
    ubicacionObjetos_ = matriz;
    Nat alto = mapa.alto();
    vector<Color> fila(alto, "");
    Nat largo = mapa.ancho();
    for (int i = 0; i < largo; i++) {
        ubicacionObjetos_.push_back(fila);
    }
    auto it1 = objetos.begin();


    while (it1 != objetos.end()) { /** Esta es la declaracion de la matriz de ubicacionObjetos */
        /** it1->second.first primer componente del significado */
        /** it1->second.second segundo componete del significado */
        ubicacionObjetos_[it1->second.first][it1->second.second] = it1->first;
        ++it1;
    }
}

bool Simulacion::hayMovimiento(Coordenada c, Direccion d) {
    bool res = true;
    if (mapa_.enRango(siguientePosicion(c, d))) {
        if (mapa_.tipoCasillero(siguientePosicion(c, d)) == ELEVACION) {
            if (mapa_.tipoCasillero(c) == PISO) {
                res = false;
            }
        }
    } else {
        res = false;
    }
    return res;
}

bool Simulacion::hayObjeto(Coordenada c) {
    return ubicacionObjetos_[c.first][c.second] != "";
}


void Simulacion::mover(Direccion d) {
    if (mapa_.enRango(siguientePosicion(posAgente(), d))) {
        cantMovimientos_++;
        if (hayMovimiento(posAgente(), d) && !(hayObjeto(siguientePosicion(posAgente(), d)))) {
            posicionAgente_ = siguientePosicion(posAgente(), d);
        } else {
            if (hayObjeto(siguientePosicion(posAgente(), d)) && hayMovimiento(siguientePosicion(posAgente(), d), d)
                && !(hayObjeto(siguientePosicion(siguientePosicion(posAgente(), d), d)))) {
                actualizarObjeto(siguientePosicion(posAgente(), d), d);
                posicionAgente_ = siguientePosicion(posAgente(), d);
                objetos_[obtenerColorObjeto(siguientePosicion(posAgente(), d))] = siguientePosicion(posAgente(), d);
                Color colorObj = obtenerColorObjeto(siguientePosicion(posAgente(), d));
                Color colorDestino = obtenerColorDestino(siguientePosicion(posAgente(), d));
                if (objetivosActuales_.count(colorObj) == 1) {
                    if (objetivosActuales_.at(colorObj).count(colorDestino) == 1) {
                        Objetivo objetivo = Objetivo(colorObj, colorDestino);
                        if (realizado(objetivo)) {
                            objetivosCompletados_++;
                            auto itConj = objetivosActuales_.at(colorObj).at(colorDestino);
                            objetivosDisponibles_.erase(itConj);
                            //objetivosDisponibles_.erase(itConj); //erase(*itConj);//Aca borramos con el iterador
                            objetivosActuales_.at(colorObj).erase(colorDestino);
                            if (objetivosActuales_.at(colorObj).size() == 0) {
                                objetivosActuales_.erase(colorObj);
                            }
                        }
                    }
                }
            }
        }
    }
}


Coordenada Simulacion::sumar(Coordenada c1, Coordenada c2) {
    Coordenada res = make_pair(0, 0);
    res.first = c1.first + c2.first;
    res.second = c1.second + c2.second;
    return res;
}

void Simulacion::actualizarObjeto(Coordenada c, Direccion d) {
    Color col = obtenerColorObjeto(c);
    Coordenada nuevaPosicion = siguientePosicion(c, d);
    ubicacionObjetos_[c.first][c.second] = "";
    ubicacionObjetos_[nuevaPosicion.first][nuevaPosicion.second] = col;
}

Coordenada Simulacion::siguientePosicion(Coordenada c, Direccion d) {
    pair<Nat, Nat> pos;
    if (d == ARRIBA) {
        pos = sumar(c, make_pair(0, 1));
    } else if (d == ABAJO) {
        if (c.second == 0) {
            pos = sumar(c, make_pair(0, mapa_.alto() + 1));
        } else {
            pos = sumar(c, make_pair(0, -1));
        }
    } else if (d == DERECHA) {
        pos = sumar(c, make_pair(1, 0));
    } else {
        if (c.first == 0) {
            pos = sumar(c, make_pair(0, mapa_.ancho()));
        } else {
            pos = sumar(c, make_pair(-1, 0));
        }
    }
    return pos;
}

Color Simulacion::obtenerColorObjeto(Coordenada c) {
    return ubicacionObjetos_[c.first][c.second];
}

Color Simulacion::obtenerColorDestino(Coordenada c) {
    return mapa_.ubicReceptaculos()[c.first][c.second];
}

bool Simulacion::realizado(Objetivo o) {
    bool res = false;
    if (objetos_.count(o.objeto()) == 1 && mapa_.receptaculos().count(o.receptaculo()) == 1) {
        if (objetos_.at(o.objeto()) == mapa_.receptaculos().at(o.receptaculo())) {
            res = true;
        }
    }
    return res;
}


void Simulacion::agregarObjetivo(Objetivo o) {
    if (coloresObjetos_.count(o.objeto()) == 1 && mapa_.receptaculos().count(o.receptaculo()) == 1) {
        if (realizado(o)) {
            objetivosCompletados_++;
        } else {
            bool estaRepetido = false; /* El codigo entre las lineas 146 y 155 se utiliza para chequar que el nuevo objetivo no este definido previamente en la lista de objetivos disponibles,
                                           en caso de que este el objetivo no se agrega a la lista. */
            pair<Color, Color> nuevoObjetivo = make_pair(o.objeto(), o.receptaculo());
            auto it = objetivosDisponibles_.begin();
            while (objetivosDisponibles_.end() != it && estaRepetido == false) {
                pair<Color, Color> objetivoEnLista = make_pair((it)->objeto(), (it)->receptaculo());
                if (nuevoObjetivo == objetivoEnLista) {
                    estaRepetido = true;
                }
                ++it;
            }
            if (estaRepetido == false) {
                objetivosDisponibles_.push_back(o);
                objetivosDisponibles_.reverse();
                auto itObjetivo = objetivosDisponibles_.begin();
                if (objetivosActuales_.count(o.objeto()) == 1) {
                    pair<Color, list<Objetivo>::iterator> objetivo = make_pair(o.receptaculo(), itObjetivo);
                    objetivosActuales_.at(o.objeto()).insert(objetivo);
                } else {
                    string_map<list<Objetivo>::iterator> nuevoDicc = string_map<list<Objetivo>::iterator>();
                    pair<Color, string_map<list<Objetivo>::iterator>> objetivoObj = make_pair(o.objeto(), nuevoDicc);
                    objetivosActuales_.insert(objetivoObj);
                    pair<Color, list<Objetivo>::iterator> objetivo = make_pair(o.receptaculo(), itObjetivo);
                    objetivosActuales_.at(o.objeto()).insert(objetivo);
                }
            }
        }
    }
}

const string_map<Coordenada> &Simulacion::posObjetos() const {
    return objetos_;
}

const Mapa Simulacion::mapa() const {
    return mapa_;
}

Coordenada Simulacion::posAgente() const {
    return posicionAgente_;
}

const list<Objetivo> &Simulacion::objetivosDisponibles() const {
    return objetivosDisponibles_;
}

Nat Simulacion::movimientos() const {
    return cantMovimientos_;
}

Nat Simulacion::objetivosResueltos() const {
    return objetivosCompletados_;
}

const set<Color> &Simulacion::coloresObjetos() const {
    return coloresObjetos_;
}