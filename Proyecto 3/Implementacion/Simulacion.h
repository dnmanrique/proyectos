#ifndef TP3_ALGO2LANDIA_SIMULACION_H
#define TP3_ALGO2LANDIA_SIMULACION_H

//#include "Tipos.h"
//#include "aed2_mapa.h"
#include "Mapa.h"
//#include "Objetivo.h"


class Simulacion {

public:
    Simulacion(const Mapa& mapa, const map<Color,Coordenada>& objetos, Coordenada inicio);

    void mover(Direccion d);

    void agregarObjetivo(Objetivo);

    const Mapa mapa() const;

    const string_map<Coordenada>& posObjetos() const; //Falta cambiar el map por el diccTrie

    Coordenada posAgente() const;

    const list<Objetivo>& objetivosDisponibles() const;

    Nat movimientos() const;

    Nat objetivosResueltos() const;

    bool realizado(Objetivo o);

    const set<Color>& coloresObjetos() const;


private:
    Coordenada sumar(Coordenada c1, Coordenada c2);
    void actualizarObjeto(Coordenada c ,Direccion d);
    Coordenada siguientePosicion(Coordenada c , Direccion d);
    Color obtenerColorObjeto(Coordenada c);
    Color obtenerColorDestino(Coordenada c);
    bool hayMovimiento(Coordenada c, Direccion d);
    bool hayObjeto(Coordenada c);

    Mapa mapa_;
    list<Objetivo> objetivosDisponibles_;
    Nat cantMovimientos_;
    Nat objetivosCompletados_;
    Coordenada posicionAgente_;
    string_map<string_map<list<Objetivo>::iterator>> objetivosActuales_; //Aca va el diccTrie objetivosActuales
    vector<vector<Color>> ubicacionObjetos_;
    string_map<Coordenada> objetos_;
    set<Color> coloresObjetos_;
};

#endif
