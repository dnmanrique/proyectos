#ifndef OBJETIVO_H
#define OBJETIVO_H
#include "Tipos.h"
using namespace std;

class Objetivo {
  public:

    Objetivo();

    Objetivo(const Color& objeto, const Color& receptaculo);

    const Color& objeto() const;
    
    const Color& receptaculo() const;
  
  private:
    friend class Simulacion;
    Color objeto_;
    Color receptaculo_;


    //completar
};

//#include "Objetivo.hpp"

#endif
