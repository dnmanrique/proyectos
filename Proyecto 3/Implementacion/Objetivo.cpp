#include "Objetivo.h"
//#include "Tipos.h"

Objetivo::Objetivo() : objeto_(" "), receptaculo_(" ") {}

Objetivo::Objetivo(const Color& objeto, const Color& receptaculo) : objeto_(objeto), receptaculo_(receptaculo) {}

const Color& Objetivo::objeto() const {
    return objeto_;
}

const Color& Objetivo::receptaculo() const {
    return receptaculo_;
}


