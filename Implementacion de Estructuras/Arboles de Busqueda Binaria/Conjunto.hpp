
template <class T>
Conjunto<T>::Nodo::Nodo(const T &v):valor(v),izq(NULL),der(NULL),padre(NULL) {}/
template <class T>
Conjunto<T>::Conjunto():_raiz(NULL),cant(0) {

}

template <class T>
Conjunto<T>::~Conjunto() { 
   Destruir(_raiz);
}
template<class T>
void Conjunto<T>::Destruir(Nodo * nodo){
    if (nodo!=NULL){
        Destruir(nodo->der);
        Destruir(nodo->izq);
        delete (nodo);
    }
}


template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo *temp=_raiz;
   while (temp != NULL) {
        if (temp->valor==clave){
            return true;
        }
        if (temp->valor < clave) {
            temp = temp->der;
            //temp->der = this->_raiz;
        } else {
            temp = temp->izq;
            //temp->izq=this->_raiz;
        }
    }


    //assert(false);
    return false;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    bool fin;
    bool dev;
    Nodo *temp=_raiz;
    if (temp==NULL) {
        Nodo *node=new Nodo(clave);
        _raiz = node;
        node->padre=NULL;
        node->izq = NULL;
        node->der = NULL;
        cant++;
    }else {
         fin=false;
        dev=false;
        while (!fin){
            if (clave<temp->valor){
                if (temp->izq==NULL){//me fijo si es nulo y agrego modifcando los punteros
                    temp->izq=new Nodo(clave);
                    temp->izq->padre=temp;
                    temp=temp->izq;
                    temp->der=NULL;
                    temp->izq=NULL;
                    fin=true;
                    dev=true;
                }
                else{
                   temp=temp->izq;//recorro por izquierda hasta encontrar el nulo
                }
            }else {
                if (clave>temp->valor){
                    if(temp->der==NULL){//idem izquierda pero con derecha
                        temp->der=new Nodo(clave);
                        temp->der->padre=temp;
                        temp=temp->der;
                        temp->der=NULL;
                        temp->izq=NULL;
                        fin=true;
                        dev=true;
                    }else{
                        temp=temp->der;
                    }
                }else{
                    fin=true;//si no es mayor ni menor es igual por lo que termino el ciclo y no cuento  
                    dev=false;
                }
                }   
        if (dev){
           cant++;//si termina el ciclo cuento uno mas
              }

}

}
}


       


template <class T>
void Conjunto<T>::remover(const T& clave) 
   
{
    Nodo *q = NULL;
    Nodo *aux = NULL;
    Nodo *temp = _raiz;

    bool borrar;

    while (temp->valor != clave) {

        if (temp->valor < clave) {
            temp = temp->der;
            //temp->der = this->_raiz;
        } else {
            temp = temp->izq;
            //temp->izq=this->_raiz;
        }
    }
    if (temp != NULL) {
        //   dev = siguiente(temp);
        cant--;
        if (temp->izq == NULL && temp->der == NULL) {//elimino nodo sin hijos
            //cout<<"1"<<endl;
            if (temp == _raiz) {
                delete (temp);
                _raiz = NULL;
            } else {
                q = temp->padre;
                delete (temp);
                if (q->izq == temp) {
                    q->izq = NULL;
                } else { q->der = NULL; }
            }
        } else if (temp->izq == NULL) {//elimino nodo con solo con hijo derecho
            //cout<<"2"<<endl;
            if (temp == _raiz) {
                _raiz = temp->der;
                delete (temp);
                _raiz->padre = NULL;
            } else {
                q = temp->padre;
                if (q->izq == temp) {
                    q->izq = temp->der;
                    if(temp->der!=NULL){
                        temp->der->padre=q;
                    }
                    delete (temp);
                } else {
                    q->der = temp->der;
                    if(temp->der!=NULL){
                        temp->der->padre=q;
                    }
                    delete (temp);
                }
            }
        } else if (temp->der == NULL) {//elimino nodo con solo con hijo izquierdo
            //cout<<"3"<<endl;
            if (temp == _raiz) {
                _raiz = temp->izq;
                delete (temp);
                _raiz->padre = NULL;
            } else {
                q = temp->padre;
                if (q->der == temp) {
                    q->der = temp->izq;
                    if(temp->izq!=NULL){
                        temp->izq->padre=q;
                    }
                    delete (temp);
                } else {
                    q->izq = temp->izq;
                    if(temp->izq!=NULL){
                        temp->izq->padre=q;
                    }
                    delete (temp);
                }
            }
        } else {
            //cout<<"4"<<endl;
            q = temp->der;
            borrar=false;
            while (q->izq!= NULL && !borrar) {//elimino nodo con los dos hijos
                //cout<<"41"<<endl;
                q = q->izq;
            }/*
                aux = q->padre;            
                if (q->der == NULL) {
                    if (aux->izq == q) {
                        aux->izq = NULL;

                    } else {
                        aux->der = NULL;
                    }
                } else {
                    if (aux->izq == q) {
                        aux->izq = q->der;

                    } else {
                        aux->der = q->der;
                        q->der->padre = aux;
                    }

                }

                q->izq = temp->izq;
                q->der = temp->der;
                q->padre = temp->padre;

                if (q->padre != NULL) {
                    aux = q->padre;
                    if (aux->izq == temp) {
                        aux->izq = q;

                    } else {
                        aux->der = q;
                    }
                } else {
                    _raiz = q;
                }
                if (q->izq != NULL) {
                    q->izq->padre = q;
                    if (q->der != NULL) {
                        q->der->padre = q;
                        delete (temp);
                        borrar=true;

                    }
                }
            }//q = temp->der;*/
            if(q->izq==NULL && !borrar){
                temp->valor=q->valor;
                aux=q->padre;
                if (aux->der==q){
                    aux->der=q->der;
                    if (q->der != NULL) {
                        q->der->padre=aux;
                    }
                    delete(q);
                }else {
                    aux->izq=q->der;
                    if (q->der != NULL) {
                        q->der->padre=aux;
                    }
                    delete(q);
                }

                /*if(temp==_raiz){
                    _raiz=q;
                    q->izq=temp->izq;
                    q->padre=NULL;
                    temp->izq->padre=q;
                    delete(temp);   
                }
                else{
                    aux=temp->padre;
                    if (aux->der==temp){
                        temp->padre->der=temp->der;
                        q->izq=temp->izq;
                        q->padre=temp->padre;
                        q->izq->padre=q;
                        delete (temp);
                    }else {
                        temp->padre->izq=temp->der;
                        q->izq=temp->izq;
                        q->padre=temp->padre;
                        q->izq->padre=q;
                        delete (temp);
                    }
                }*/
            }
        }
    }    
}



template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {

    Nodo *padre;
    bool subir;

    Nodo *temp = _raiz;

    while (temp->valor != clave) {

        if (temp->valor < clave) {
            temp = temp->der;
            //temp->der = this->_raiz;
        } else {
            temp = temp->izq;
            //temp->izq=this->_raiz;
        }
    }


    if (temp->der != NULL) {
        temp = temp->der;
        while (temp->izq != NULL) {
            temp = temp->izq;
        }
        return temp->valor;
        //return  this->minimo();
    } else {
        subir = true;
        while (subir) {
            padre = temp->padre;
            if (padre == NULL) {
                temp = NULL;
                subir = false;
            } else if (padre->der != temp) {
                temp = padre;
                subir = false;
            } else {
                temp = padre;
            }

        }
    }
    return temp->valor;
}



template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo *temp=_raiz;
    while(temp->izq!=NULL){
        temp=temp->izq;
    }
    return temp->valor;
    //assert(false);

}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo *temp=_raiz;
    while (temp->der!=NULL){
        temp=temp->der;
    }
    return temp->valor;
    //assert(false);

}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    //assert(false);
    int res=this->cant;
    return res;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}
