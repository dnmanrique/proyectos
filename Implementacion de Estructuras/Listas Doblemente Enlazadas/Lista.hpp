#include "Lista.h"

Lista::Nodo::Nodo(const int &elem):valor(elem),anterior(NULL),siguiente(NULL){}



Lista::Lista():_primero(NULL),_ultimo(NULL),lenght(0) {
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    this->Borrar();
}
void Lista::Borrar() {
    Nodo *temp=_primero;
    while (temp!=NULL)
    {
        temp=temp->siguiente;
        delete (_primero);
        _primero=temp;
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    this->Borrar();
    _primero=NULL;
    _ultimo=NULL;
    lenght=0;
    Nodo *temp=aCopiar._primero;
    while (temp!=NULL){
        agregarAtras(temp->valor);
        temp=temp->siguiente;
    }

    // Completar
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo *nodo=new Nodo(elem);
    if (lenght!=0){
        nodo->siguiente=this->_primero;
        nodo->anterior=NULL;
        _primero->anterior=nodo;
        _primero=nodo;
        lenght++;

    }
    else{
        nodo->anterior=NULL;
        nodo->siguiente=NULL;
        _primero=nodo;
        _ultimo=nodo;
        lenght++;
    }
    // Completar
}

void Lista::agregarAtras(const int& elem) {
    Nodo *nodo=new Nodo(elem);
    if (lenght!=0){
        nodo->siguiente=NULL;
        nodo->anterior=this->_ultimo;
        _ultimo->siguiente=nodo;
        _ultimo=nodo;
        lenght++;
    }else{
        nodo->siguiente=NULL;
        nodo->anterior=NULL;
        _primero=nodo;
        _ultimo=nodo;
        lenght++;
    }
    // Completar
}

void Lista::eliminar(Nat i) {
    int n=0;
    Nodo *temp=_primero;
    if (i==0)
    {
        _primero=temp->siguiente;
        temp->siguiente=NULL;
        delete(temp);
        this->lenght--;
    }else if (i==lenght-1)
    {
        Nodo *temp=this->_ultimo;
        _ultimo=temp->anterior;
        _ultimo->siguiente=NULL;
        temp->anterior=NULL;
        delete(temp);
        this->lenght--;

    }else{
        while(n<i && temp!=NULL){
            temp=temp->siguiente;
            n++;
            if (n==i){
                temp->anterior->siguiente=temp->siguiente;
                temp->siguiente->anterior=temp->anterior;
                temp->anterior=NULL;
                temp->siguiente=NULL;
                delete(temp);
                this->lenght--;
            }
        }
    }
    // Completar
}

int Lista::longitud() const {
    int res=this->lenght;
    return res;
}

const int& Lista::iesimo(Nat i) const {
    Nodo *temp=this->_primero;
    int n=0;
    while(i!=n)
    {
        temp=temp->siguiente;
        i++;
    }
    // Completar
    return temp->valor;
}

int& Lista::iesimo(Nat i) {
    Nodo *temp=this->_primero;
    int cont=0;
    while(i!=cont)
    {
        temp=temp->siguiente;
        cont++;
    }
    // Completar (hint: es igual a la anterior...)
    return (temp->valor);
}

void Lista::mostrar(ostream& o) {
    // Completar
}
