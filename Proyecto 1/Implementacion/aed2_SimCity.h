#ifndef AED2_SIMCITY_H
#define AED2_SIMCITY_H

#include <iostream>
#include <set>
#include "aed2_Mapa.h"
#include "SimCity.h"

using namespace std;


using Nat = unsigned int;

using SimCity = SimCity;/* Completar con su clase implementada de SimCity */

class aed2_SimCity {
public:

    // Generadores:

    aed2_SimCity(aed2_Mapa m);

    // Precondicion: Se puede construir en la Casilla c
    void agregarCasa(Casilla c);

    // Precondicion: Se puede construir en la Casilla c
    void agregarComercio(Casilla c);

    // Precondicion: Hubo construccion en el turno actual
    void avanzarTurno();

    // Precondicion: No se solapan las construcciones con los rios
    //  ni las construcciones de nivel maximo de ambas partidas
    void unir(aed2_SimCity sc);

    //operacionesAuxiliares
    // Observadores:

    Mapa mapa() const;//aca es Mapa fijarse que onda con esta parte

    set<Casilla> casas() ; //antes era const

    set<Casilla> comercios()  ; //antes era const

    // Precondicion: Hay construccion en la Casilla p
    Nat nivel(Casilla  c) ;

    bool huboConstruccion() const;

    Nat popularidad() const;

    Nat antiguedad() const;

    // Conversiones
    
    // Esta función sirve para convertir del SimCity de la cátedra al SimCity
    // implementado por ustedes
    SimCity simCity();

private:
    SimCity _miSimCity;
    /*Nat _popularidad;
    bool _huboConstruccion;
    Nat _turno;
    Mapa _mapa;
    set<Casilla> _casas;
    set<Casilla> _comercios;
    set<CasillaN> _niveles;*/
    //falta algo para los niveles;
    // Completar
};

#endif // AED2_SIMCITY_H
