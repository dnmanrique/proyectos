#ifndef SIMCITY_H
#define SIMCITY_H

#include <iostream>
#include <set>
#include "Mapa.h"
#include <list>

using namespace std;


using Nat = unsigned int;

//using SimCity = /* Completar con su clase implementada de SimCity */

class SimCity {
public:

    // Generadores:

    SimCity(Mapa m);

    // Precondicion: Se puede construir en la Casilla c
    void agregarCasa(CasillaN c);

    // Precondicion: Se puede construir en la Casilla c
    void agregarComercio(CasillaN c);

    // Precondicion: Hubo construccion en el turno actual
    void avanzarTurno();

    // Precondicion: No se solapan las construcciones con los rios
    //  ni las construcciones de nivel maximo de ambas partidas
    void unir(SimCity sc);

    //Operaciones Auxiliares
    void EliminarRepetidos(list<CasillaN> &Construcciones);
    bool Esta(list<CasillaN> Construcciones,CasillaN C ) const;
    void Unir(list<CasillaN> &C1, list<CasillaN> C2);
    bool EstaADistanciaManhattan(CasillaN C, CasillaN C2) const;
    list<CasillaN> CasasADistanciaManhattan(CasillaN C) const ;
    //esto es para la funcion que pasa del tad de la catedra al nuestro
    void ActualizarTurno(Nat T);
    void ActualizarPopularidad(Nat T);
    void ActualizarConstruccion(bool T);
    void SwapNiveles(CasillaN C);


    // Observadores:

    Mapa mapa() const;

    list<CasillaN> casas() ; //Estas antes era const

    list<CasillaN> comercios()  ; //esta antes era const

    // Precondicion: Hay construccion en la Casilla p
    Nat nivel(Casilla c) ;

    bool huboConstruccion() const;

    Nat popularidad() const;

    Nat verTurnoActual() const;

    // Conversiones
    
    // Esta función sirve para convertir del SimCity de la cátedra al SimCity
    // implementado por ustedes
    //SimCity simCity();

private:
    Nat _popularidad;
    bool _huboConstruccion;
    Nat _turno;
    Mapa _mapa;
    list<CasillaN> _casas;
    list<CasillaN> _comercios;
    list<CasillaN> _niveles;

    // Completar
};

#endif // AED2_SIMCITY_H
