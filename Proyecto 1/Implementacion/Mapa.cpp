#include "Mapa.h"
Mapa ::Mapa(){
    _horizontales= set<Nat>();
    _verticales = set<Nat>();
}
void Mapa::agregarRio(Direccion d, int p) {
   if (d==Horizontal) {
        _horizontales.insert(p);
   }else{
       _verticales.insert(p);
   }
}
void Mapa::unirMapa(Mapa m2) { //Considero unirMapa en O(1)
    for (Nat V:m2._verticales)
    {
        _verticales.insert(V);
    }
    for (Nat H:m2._horizontales)
    {
        _horizontales.insert(H);
    }
}

Mapa & Mapa::operator=(const Mapa &m) {
    if (this!=&m)
    {
        this->_verticales=m._horizontales;
        this->_horizontales=m._horizontales;
    }
    return *this;
}
bool Mapa::hayRio(Casilla c) const {
  return _verticales.count(c.first) || _horizontales.count(c.second);

}
set<Nat> Mapa::verHorizontales() {
    return _horizontales;
}
set<Nat> Mapa::verVerticales() {
    return _verticales;
}
// Completar
