#include "aed2_Mapa.h"

aed2_Mapa::aed2_Mapa() {
    Mapa m;
    _miMapa=m;


}
aed2_Mapa::aed2_Mapa(Mapa m) {
    for (Nat v:m.verHorizontales()){
        _miMapa.agregarRio(Horizontal,v);
    }
    for (Nat v:m.verVerticales()){

        _miMapa.agregarRio(Vertical,v);
    }

}
void aed2_Mapa::agregarRio(Direccion d, int p) {

        _miMapa.agregarRio(d,p);
    }


bool aed2_Mapa::hayRio(Casilla c) const {
    return _miMapa.hayRio(c);
}

void aed2_Mapa::unirMapa(aed2_Mapa m2) {
    _miMapa.unirMapa(m2.mapa());
    /*set<Nat>::iterator it;
    set<Nat>::iterator it2;
    it2=m2._horizontales.begin();
    it=m2._verticales.begin();
    while (it!=m2._verticales.end())
    {
        _verticales.insert(*it);
        it++;
    }
    while(it2!=m2._horizontales.end())
    {
        _horizontales.insert(*it2);
    }*/
    /*for (Nat V:m2._miMapa.verVerticales())
    {
        _miMapa.agregarRio(Vertical,V);
    }
    for (Nat H:m2._miMapa.verHorizontales())
    {
        _miMapa.agregarRio(Horizontal,H);
    }*/
}

Mapa aed2_Mapa::mapa() {
    Mapa m;
    for( Nat v:this->_miMapa.verVerticales())   {
        m.agregarRio(Vertical,v);
    }
    for (Nat h:this->_miMapa.verHorizontales())
    {
        m.agregarRio(Horizontal,h);
    }

    return m;
}
// Completar
