#ifndef TIPOS_H
#define TIPOS_H

using namespace std;
using Casilla = pair<int,int >; //aca tenemos en la primera posicion de la tupla la posicion
using CasillaN= pair<pair<int, int>,int > ;                                       // en el mapa y en la segunda posicion el nivel para esa casilla
enum Direccion {
    Horizontal, Vertical
};

#endif
