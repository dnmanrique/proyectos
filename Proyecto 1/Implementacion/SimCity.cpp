#include "SimCity.h"
SimCity ::SimCity(Mapa m) :_popularidad(0),_huboConstruccion(false),_turno(0),_mapa(m){
    _niveles= list<CasillaN> ();
    _casas = list <CasillaN> ();
    _comercios = list<CasillaN> ();

}
bool SimCity::Esta(list<CasillaN> Construcciones, CasillaN C) const {
    for (CasillaN Constr:Construcciones)
    {
        if (Constr==C)
        {
            return true;
        }
    }
    return false;
}
void SimCity::EliminarRepetidos(list<CasillaN> &Construcciones) {
    set<CasillaN> temp=set<CasillaN>();
    for (CasillaN C:Construcciones)
    {
        temp.insert(C);
    }
    set<CasillaN> temp1=temp;
    for (CasillaN c2:temp1)
    {
        for (CasillaN ca:temp1)
        {
            if (c2.first==ca.first)
            {
                if (c2.second>ca.second)
                {
                    temp.erase(ca);
                    _niveles.remove(ca);
                } else if (c2.second<ca.second)
                {
                    temp.erase(c2);
                    _niveles.remove(c2);
                }
            }
        }
    }

    for (CasillaN F:temp)
    {
        Construcciones.remove(F);
        Construcciones.push_back(F);
        _niveles.remove(F);
        _niveles.push_back(F);
    }
}
void SimCity::SwapNiveles(CasillaN C) {
    list<CasillaN> NivelesAux=_niveles;
    for (CasillaN C1:NivelesAux)
    {
        if (C.first==C1.first)
        {
            _niveles.remove(C1);
            _niveles.push_back(C);
        }
    }
}

void SimCity::ActualizarTurno(Nat T) {
    _turno=T;
}
void SimCity::ActualizarConstruccion(bool T) {
    _huboConstruccion=T;
}
void SimCity::ActualizarPopularidad(Nat T) {
    _popularidad=T;
}
void SimCity::agregarCasa(CasillaN c) {
    _casas.push_back(c);
    _huboConstruccion=true;
    c.second=0;
    _niveles.push_back(c);

}
void SimCity::agregarComercio(CasillaN c) {
    _comercios.push_back(c);
    _huboConstruccion= true;
    c.second=0;
    _niveles.push_back(c);
}

void SimCity::Unir(list<CasillaN> &C1, list<CasillaN> C2) {

    C1.merge(C2);
}

void SimCity::unir(SimCity sc) {

    _huboConstruccion=_huboConstruccion ||sc._huboConstruccion;
    _turno=max(_turno,sc._turno);
    _popularidad=_popularidad+sc._popularidad+1;
    Unir(_casas,sc._casas);
    Unir(_comercios,sc._comercios);
    _mapa.unirMapa(sc._mapa);
    Unir(_niveles,sc._niveles);
}


void SimCity::avanzarTurno() {
    _turno = _turno + 1;
    _huboConstruccion = false;

    EliminarRepetidos(_casas);
    EliminarRepetidos(_comercios);
    list<CasillaN> CasasAux=_casas;
    list<CasillaN> ComerciosAux=_comercios;
    for (CasillaN casas:CasasAux) {
        for (CasillaN comercios:ComerciosAux) {
            if (casas.first == comercios.first) {
                if (casas.second==comercios.second) {
                    _comercios.remove(comercios);
                    _niveles.remove(comercios);
                    _niveles.push_back(casas);
                }else{
                    _comercios.remove(comercios);
                    _niveles.remove(comercios);
                }
                }
            }
        }
    //Aca actualizo los niveles;
    list<CasillaN> niveles2=_niveles;
    for (CasillaN construccion:niveles2) {
        if (Esta(_casas, construccion)) {
            _casas.remove(construccion);
            _niveles.remove(construccion);
            construccion.second = construccion.second + 1;
            _casas.push_back(construccion);
            _niveles.push_back(construccion);
        } else if (Esta(_comercios, construccion)) {
            bool huboCambios=false;
            if (CasasADistanciaManhattan(construccion).empty()) {
                _comercios.remove(construccion);
                _niveles.remove(construccion);
                construccion.second = construccion.second + 1;
                _comercios.push_back(construccion);
                _niveles.push_back(construccion);
            } else {
                for (CasillaN CasasC:CasasADistanciaManhattan(construccion)) {
                    if (construccion.second < CasasC.second) {
                        huboCambios=true;
                        _comercios.remove(construccion);
                        _niveles.remove(construccion);
                        construccion.second =CasasC.second;
                        _comercios.push_back(construccion);
                        _niveles.push_back(construccion);
                    }
                }
                if (!huboCambios){
                    _comercios.remove(construccion);
                    _niveles.remove(construccion);
                    construccion.second = construccion.second + 1;
                    _comercios.push_back(construccion);
                    _niveles.push_back(construccion);
                }
            }
        }
    }
}



bool SimCity::EstaADistanciaManhattan(CasillaN C, CasillaN C2) const {
    int DistanciaX= C.first.first-C2.first.first;
    int DistanciaY=C.first.second-C2.first.second;
    if (DistanciaX<0)
    {
        DistanciaX=DistanciaX*(-1);
    }
    if (DistanciaY<0)
    {
        DistanciaY=DistanciaY*(-1);
    }
    return DistanciaX+DistanciaY <= 3;
}

list<CasillaN> SimCity::CasasADistanciaManhattan(CasillaN C) const {
    list<CasillaN> ListadeCasas;
    for (CasillaN Casa:_casas)
    {
        if (EstaADistanciaManhattan(C,Casa))
        {
            ListadeCasas.push_back(Casa);
        }
    }
    return ListadeCasas;
}
Nat SimCity::nivel(Casilla c)
{

    for (CasillaN C1:_niveles){
      if (c==C1.first)
      {
          if (Esta(_casas,C1))
          {
              return C1.second;
          } else
          {
              if (CasasADistanciaManhattan(C1).empty())
              {
                  return C1.second;
              }
              else
              {
                  for (CasillaN CasasC:CasasADistanciaManhattan(C1))
                  {
                      if (C1.second<CasasC.second)
                      {
                          _comercios.remove(C1);
                          _niveles.remove(C1);
                          C1.second = CasasC.second;
                          _comercios.push_back(C1);
                          _niveles.push_back(C1);

                      }
                  }
                  return C1.second;

              }
          }
      }
  }
}

list<CasillaN> SimCity::casas()  {
    if (_casas.empty())
    {
        return _casas;
    } else{
        EliminarRepetidos(_casas);

        list<CasillaN> CasasAux=_casas;
        list<CasillaN> ComerciosAux=_comercios;
        for (CasillaN casas:CasasAux) {
            for (CasillaN comercios:ComerciosAux) {
                if (casas.first == comercios.first) {

                        _comercios.remove(comercios);
                        _niveles.remove(comercios);
                    }
                }
            }
        }

    return _casas;
}
list<CasillaN> SimCity::comercios() {
    if (_comercios.empty()) {
        return _comercios;
    } else {

        EliminarRepetidos(_comercios);
        list<CasillaN> CasasAux = _casas;
        list<CasillaN> ComerciosAux = _comercios;
        for (CasillaN casas:CasasAux) {
            for (CasillaN comercios:ComerciosAux) {
                if (casas.first == comercios.first) {

                        _comercios.remove(comercios);
                        _niveles.remove(comercios);
                }
            }
        }

        return _comercios;
    }
}

bool SimCity::huboConstruccion() const {
    return _huboConstruccion;
}
Mapa SimCity::mapa() const {
    return _mapa;
}

Nat SimCity::verTurnoActual() const { //Este es antiguedad en el del la catedra
    return _turno;
}

Nat SimCity::popularidad() const {
    return _popularidad;
}

