#ifndef MAPA_H
#define MAPA_H

#include <iostream>
#include "Tipos.h"
#include <set>

using Nat = unsigned int;
using namespace std;

//using Mapa = /* Completar con la clase Mapa implementada por ustedes */


class Mapa {
public:

    // Generadores:

    Mapa();

    void agregarRio(Direccion d, int p);

    // Observadores:

    bool hayRio(Casilla c) const;

    // Otras operaciones:

    void unirMapa(Mapa m2);

    set<Nat> verHorizontales();

    set<Nat> verVerticales();
    // Conversiones
    Mapa& operator =(const Mapa &m);
    // Esta función sirve para convertir del Mapa implementado por ustedes
    // a la clase Mapa de la cátedra
    //aed2_Mapa(Mapa m);

    // Esta función sirve para convertir del Mapa de la cátedra (aed2_Mapa)
    // a la clase Mapa definida por ustedes
    //Mapa mapa();

private:
    set<Nat> _horizontales;
    set<Nat> _verticales;// Completar
};

#endif // AED2_MAPA_H
