#include "aed2_SimCity.h"
aed2_SimCity::aed2_SimCity(aed2_Mapa m) :_miSimCity(m.mapa()){

}

void aed2_SimCity::agregarCasa(Casilla c) {
    CasillaN Nuevo;
    Nuevo.first=c;
    Nuevo.second=0;
    _miSimCity.agregarCasa(Nuevo);
}
void aed2_SimCity::agregarComercio(Casilla c) {
    CasillaN Nuevo;
    Nuevo.first=c;
    Nuevo.second=0;
    _miSimCity.agregarComercio(Nuevo);
}

void aed2_SimCity::avanzarTurno() {
    _miSimCity.avanzarTurno();


}

Nat aed2_SimCity::nivel(Casilla c) {

    return _miSimCity.nivel(c);
}


void aed2_SimCity::unir(aed2_SimCity sc) {
    _miSimCity.unir(sc.simCity());
}
SimCity aed2_SimCity::simCity() {
    SimCity simCity1(_miSimCity.mapa());
    simCity1.ActualizarPopularidad(_miSimCity.popularidad());
    simCity1.ActualizarTurno(_miSimCity.verTurnoActual());
    bool huboConstruccion=  _miSimCity.huboConstruccion();

    for (CasillaN C:_miSimCity.casas()){
            simCity1.agregarCasa(C);
            simCity1.SwapNiveles(C);
        }
    for (CasillaN C1:_miSimCity.comercios())
        {
            simCity1.agregarComercio(C1);
            simCity1.SwapNiveles(C1);
        }
    simCity1.ActualizarConstruccion(huboConstruccion);
    return simCity1;
}



Nat aed2_SimCity::antiguedad() const {
    return _miSimCity.verTurnoActual();
}

Mapa aed2_SimCity::mapa() const {

    return _miSimCity.mapa();
}
bool aed2_SimCity::huboConstruccion() const {
    return _miSimCity.huboConstruccion();
}
set<Casilla> aed2_SimCity::casas() {
     set<Casilla> Casas1;
     for (CasillaN C:_miSimCity.casas()){
         Casas1.insert(C.first);
     }
     return Casas1;

}
set<Casilla> aed2_SimCity::comercios()   {
    set<Casilla> Comercios1;
    for (CasillaN C:_miSimCity.comercios())
    {
        Comercios1.insert(C.first);
    }
    return Comercios1;
}
Nat aed2_SimCity::popularidad() const {
    return _miSimCity.popularidad();
}

