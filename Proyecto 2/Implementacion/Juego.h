#ifndef JUEGO_H
#define JUEGO_H

#include "Sokoban.h"

class Juego{
    public:
        Juego(list<Sokoban> Niveles);
        void MoverJugador(Direccion d);
        void TirarBomba();
        void Deshacer();
        Coordenada PosicionActual() const;
        Nat CantidadDeBombas() const;
        bool hayCajaEnJuego(Coordenada c) const;
        bool hayParedEnJuego(Coordenada c) const;
        bool hayDepositoEnJuego(Coordenada c) const;
        bool PuedeMoverEnJuego(Direccion d) const;
        int NivelesRestantes();
        Juego operator=(Juego j);
    private:
        list<Sokoban> _nivelesPendientes;
        Sokoban _nivelActual;
};

#endif