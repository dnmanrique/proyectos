#include "aed2_Juego.h"

aed2_Juego::aed2_Juego(vector<aed2_Nivel> ns) : juego(vectorToList(ns)) {}

bool aed2_Juego::hayPared(Coordenada c) const  {
    bool res = juego.hayParedEnJuego(c);

    return res;
}

bool aed2_Juego::hayDeposito(Coordenada c) const {
    return juego.hayDepositoEnJuego(c);
}

bool aed2_Juego::hayCaja(Coordenada c) const {
    return juego.hayCajaEnJuego(c) ;
}

Coordenada aed2_Juego::posicionActual() const {
    return juego.PosicionActual();
} 

Nat aed2_Juego::cantidadBombas() const {
    return juego.CantidadDeBombas();
}

bool aed2_Juego::sePuedeMover(PuntoCardinal pc) const {
    Direccion d = toDireccion(pc);
    return juego.PuedeMoverEnJuego(d);
}

// IMPORTANTE: Debe devolver:
// - true si al mover se completa el nivel actual
// - false en caso contrario.
bool aed2_Juego::mover(PuntoCardinal pc) {
    int cantNiveles = juego.NivelesRestantes();
    Direccion d = toDireccion(pc);
    juego.MoverJugador(d);
    if(juego.NivelesRestantes() != cantNiveles){
        return true;
    }
    return false;
}

void aed2_Juego::tirarBomba() {
      juego.TirarBomba();
}

void aed2_Juego::deshacer() {
     juego.Deshacer();
}



Sokoban aed2_Juego::nivelToSoko(aed2_Nivel& n){
    Mapa mapa;
    list<Coordenada> cajasSoko;

    set<Coordenada> paredes = n.paredes;
    set<Coordenada> depositos = n.depositos;
    set<Coordenada> cajas = n.cajas;
    Coordenada pos = n.posicionInicial;
    Nat cantBombas = n.cantidadBombas;
    for(set<Coordenada>::iterator it = paredes.begin(); it != paredes.end(); ++it){
        mapa.AgPared(*it);
    }

    for(set<Coordenada>::iterator it = depositos.begin(); it != depositos.end(); ++it){
        mapa.AgDeposito(*it);
    }

    for(set<Coordenada>::iterator it = cajas.begin(); it != cajas.end(); ++it){
        cajasSoko.push_back(*it);
    }

    return Sokoban(mapa, pos, cajasSoko, cantBombas);
}
    
list<Sokoban> aed2_Juego::vectorToList(vector<aed2_Nivel>& v){
    list<Sokoban> niveles;
    for(int i = 0; i < v.size(); i++){
        niveles.push_back(nivelToSoko(v[i]));
    }
    return niveles;
}

Direccion aed2_Juego::toDireccion(const PuntoCardinal p) const{
    if(p == Norte){
        return Direccion("Norte");
    }
    else if(p == Sur){
        return Direccion("Sur");
    }
    else if(p == Oeste){
        return Direccion("Oeste");
    }
    else{
        return Direccion("Este");
        }
}