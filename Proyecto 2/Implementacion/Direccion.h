#ifndef DIRECCION_H
#define DIRECCION_H

#include "Tipos.h"
#include <string>

class Direccion{
    public:
        Direccion(string s);
        Direccion() = default;
        string toString(Direccion d);
        Coordenada operator+(Coordenada c);
    private:
        string dir;
};


#endif