#include "Juego.h"

Juego::Juego(list<Sokoban> Niveles) : _nivelActual(Niveles.front()){
    Niveles.pop_front();
    _nivelesPendientes = Niveles;
}

Juego Juego::operator=(Juego j){
    _nivelActual = j._nivelActual;
    _nivelesPendientes = j._nivelesPendientes;
    return *this;
}

void Juego::MoverJugador(Direccion d){
    _nivelActual.MoverJugadorEnSoko(d);
    if(_nivelActual.Gano()){
        if(_nivelesPendientes.size() != 0){
            _nivelActual = _nivelesPendientes.front();
            _nivelesPendientes.pop_front();
        }
        else{
            cout << "Se gano el juego" << endl;
        }
    }
}

void Juego::TirarBomba(){
    _nivelActual.TirarBombaEnSoko();
}

void Juego::Deshacer(){
    _nivelActual.DeshacerEnSoko();
}

Coordenada Juego::PosicionActual() const{
    return _nivelActual.CoordenadaActual();
}

Nat Juego::CantidadDeBombas() const{
    return _nivelActual.Bombas();
}

bool Juego::hayCajaEnJuego(Coordenada c) const{
    return  _nivelActual.hayCajaEnSoko(c);
}


bool Juego::hayParedEnJuego(Coordenada c) const{
    return _nivelActual.hayParedEnSoko(c);
}

bool Juego::hayDepositoEnJuego(Coordenada c) const{
    return _nivelActual.hayDepositoEnSoko(c);
}

bool Juego::PuedeMoverEnJuego(Direccion d) const{
    return _nivelActual.PuedoMover(d);
} 

int Juego::NivelesRestantes(){
    return _nivelesPendientes.size() + 1;
}