#ifndef SOKOBAN_H
#define SOKOBAN_H

#include "Direccion.h"
#include "Mapa.h"
#include <list>
#include <vector>
#include <stack>

class Sokoban{
    public:
        Sokoban(Mapa m, Coordenada p, list<Coordenada> cs, Nat b);
        void MoverJugadorEnSoko(Direccion d);
        void TirarBombaEnSoko();
		bool PuedoMover(Direccion d) const;
        bool Gano();
		void DeshacerEnSoko();
        bool hayParedEnSoko(Coordenada p) const;
        bool hayDepositoEnSoko(Coordenada c) const;
        Nat Bombas() const;
        Coordenada CoordenadaActual() const;
        bool hayCajaEnSoko(Coordenada) const;
	private:        
        struct Hist{
            public:
                Hist() = default;
                Coordenada coordAnterior;
                Direccion direccion;
                bool tiroBomba;
                bool movioCaja;
                Coordenada nuevaPosCaja;
                Nat cajasADepAnterior;
                void operator=(Hist h){
                    coordAnterior = h.coordAnterior;
                    direccion = h.direccion; 
                    tiroBomba = h.tiroBomba;
                    movioCaja = h.movioCaja;
                    nuevaPosCaja = h.nuevaPosCaja;
                    cajasADepAnterior= h.cajasADepAnterior;
                }
		};

        Coordenada _coordActual;
        Mapa _mapa;
        Nat _cantBombas;
		list<Coordenada> _Cajas;
		Nat _cajasADepositar;
        stack<Hist> _Movimientos;
        bool Pertenece(const list<Coordenada>& l, Coordenada c) const;
        void EliminarCaja(list<Coordenada> &L,Coordenada c);
};


#endif