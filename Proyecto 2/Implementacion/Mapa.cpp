#include "Mapa.h"

Mapa::Mapa(){}

bool Mapa::AgPared(Coordenada p){
    return agregarOrdenado(_paredes, p);
}

bool Mapa::AgDeposito(Coordenada d){
    return agregarOrdenado(_depositos, d);
}

bool Mapa::hayPared(Coordenada p) const{
    bool habiaPared = LogPertenece(_paredes, p);
    bool seTiroBomba = false;
    for(list<Coordenada>::const_iterator it = _bombasArrojadas.begin(); it != _bombasArrojadas.end(); ++it){
        if((*it).first == p.first || (*it).second == p.second){
            seTiroBomba = true;
        }
    }
    return (habiaPared && !seTiroBomba);
}

bool Mapa::hayDeposito(Coordenada d) const{
    return LogPertenece(_depositos, d);
}

void Mapa::tirarBomba(Coordenada b){
    _bombasArrojadas.push_back(b);
}

vector<Coordenada>& Mapa::Paredes(){
    return _paredes;
}

vector<Coordenada>& Mapa::Depositos(){
    return _depositos;
}

bool Mapa::LogPertenece(const vector<Coordenada>& v, Coordenada c) const{
    bool res = false;
    if(v.size() != 0){
        int desde = 0;
        int hasta = v.size() - 1;
        while(desde <= hasta && !res){
            int medio = desde + (hasta - desde)/2;
            if(v[medio] == c){
                res = true;
            }
            else if(esMenorCoord(v[medio], c)){
                desde = medio + 1;
            }
            else{
                hasta = medio - 1;
            }
        }
    } 
    return res;
}

bool Mapa::seTiroBombaAhi(Coordenada c){
    bool res = false;
    for(list<Coordenada>::iterator it = _bombasArrojadas.begin(); it != _bombasArrojadas.end(); ++it){
        if((*it).first == c.first || (*it).second == c.second){
            res = true;
        }
    }
    return res;
}

void Mapa::deshacerBomba(){
    _bombasArrojadas.pop_back();
}

bool Mapa::agregarOrdenado(vector<Coordenada>& v, Coordenada c){
    bool res = false;
    if(!hayPared(c) && !hayDeposito(c)){
        res = true;
        int i = 0;
        while(i < v.size() && esMenorCoord(v[i], c)){
            i++;
        }
        if(i == v.size()){
            v.push_back(c);
        }
        else{
            int j = v.size() - 1;
            v.push_back(v[j]);
            while(j > i){
                v[j] = v[j - 1];
                j--;
            }
            v[j] = c;
        }
    }
    return res;
}

bool Mapa::esMenorCoord(Coordenada c, Coordenada b) const{
    if(c.first < b.first){
        return true;
    }
    else if (c.first == b.first){
        if(c.second < b.second){
            return true;
        }
    }
    return false;
}