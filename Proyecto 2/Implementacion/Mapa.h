#ifndef MAPA_H
#define MAPA_H

#include "Direccion.h"
#include <list>
#include <vector>

class Mapa{
    public:
        Mapa();
        bool AgPared(Coordenada p);
        bool AgDeposito(Coordenada d);
        bool hayPared(Coordenada p) const;
        bool hayDeposito(Coordenada d) const;
        void tirarBomba(Coordenada b);
        vector<Coordenada>& Paredes();
        vector<Coordenada>& Depositos();
        bool LogPertenece(const vector<Coordenada>& v, Coordenada c) const;
        bool seTiroBombaAhi(Coordenada c);
        void deshacerBomba();
    private:
        vector<Coordenada> _paredes;
        vector<Coordenada> _depositos;
        list<Coordenada> _bombasArrojadas;
        bool agregarOrdenado(vector<Coordenada>& v, Coordenada c);
        bool esMenorCoord(Coordenada c, Coordenada b) const;
};

#endif