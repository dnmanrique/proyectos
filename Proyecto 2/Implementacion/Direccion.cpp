#include "Direccion.h"

Direccion::Direccion(string s) : dir(s) {}

string Direccion::toString(Direccion d){
    return dir;
}

Coordenada Direccion::operator+(Coordenada c){
    if(toString(*this) == "Norte"){
        return make_pair(c.first, c.second + 1);
    }
    else if(toString(*this) == "Sur"){
        return make_pair(c.first, c.second - 1);
    }
    else if(toString(*this) == "Este"){
        return make_pair(c.first + 1, c.second);
    }
    else{
        return make_pair(c.first - 1, c.second);
    }

}