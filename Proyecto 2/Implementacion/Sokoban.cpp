#include "Sokoban.h"

Sokoban::Sokoban(Mapa mapa, Coordenada p, list <Coordenada>cs, Nat b){
	_mapa = mapa;
	_coordActual = p;
	_Cajas = cs;
	_cantBombas = b;
	_cajasADepositar = 0;
	for(list<Coordenada>::iterator it = cs.begin(); it != cs.end(); ++it){
		if(!hayDepositoEnSoko(*it)){
			_cajasADepositar++;
		}
	}
	//Falta ver el stack
}

Nat Sokoban::Bombas() const{
		return _cantBombas;
	
}

Coordenada Sokoban::CoordenadaActual() const{
		return _coordActual;
	
}

bool Sokoban::hayCajaEnSoko(Coordenada coor1) const{
		return Pertenece(_Cajas,coor1);

}

void Sokoban::TirarBombaEnSoko(){
	Hist historial;
	historial.coordAnterior = _coordActual;
	historial.direccion = Direccion("Norte");
	historial.tiroBomba = true;
	historial.movioCaja = false;
    historial.nuevaPosCaja = {0, 0};
    historial.cajasADepAnterior = _cajasADepositar;
	_Movimientos.push(historial);
	_mapa.tirarBomba(_coordActual);
	_cantBombas--;
}

bool Sokoban::Gano(){
	if (_cajasADepositar==0){
		return true;
	}else
	{
		return false;
	}
}

void Sokoban::MoverJugadorEnSoko(Direccion d){
	Hist historial;
	if (Pertenece(_Cajas, d + _coordActual)){
		historial.coordAnterior=_coordActual;
		historial.direccion=Direccion(d);
		historial.tiroBomba=false;
		historial.movioCaja=true;
		historial.nuevaPosCaja = d + _coordActual;
		historial.cajasADepAnterior=_cajasADepositar;
		_Movimientos.push(historial);
		EliminarCaja(_Cajas, d + _coordActual);
		_Cajas.push_back(d + (d + _coordActual));
		if (hayDepositoEnSoko(d + _coordActual))
		{
				_cajasADepositar++;
		}
		if (hayDepositoEnSoko(d + (d + _coordActual))){
			_cajasADepositar--;
		}

	}else
	{
		historial.coordAnterior=_coordActual;
		historial.direccion=Direccion(d);
		historial.tiroBomba=false;
		historial.movioCaja=false;
		historial.nuevaPosCaja = {0, 0};
		historial.cajasADepAnterior=_cajasADepositar;
		_Movimientos.push(historial);	
	}

	_coordActual = d + _coordActual; 
}

bool Sokoban::PuedoMover(Direccion d) const{
	Coordenada coor1 = (d + _coordActual);
    Coordenada coor2 = d + (coor1);
	if (hayParedEnSoko(coor1)){
		return false;
	}
	else
	{
		if (Pertenece(_Cajas,coor1))
		{
			if (Pertenece(_Cajas,coor2) || hayParedEnSoko(coor2))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}
}

void Sokoban::DeshacerEnSoko(){
	Hist Anterior = _Movimientos.top();
	if (Anterior.tiroBomba==true){
		_mapa.deshacerBomba();
		_cantBombas++;
	}
	else
	{
		
		if(Anterior.movioCaja == true){
			if(_Cajas.front() == Anterior.nuevaPosCaja){
				_Cajas.pop_front();
				_Cajas.push_front(_coordActual);
			}
			else{
				_Cajas.pop_back();
				_Cajas.push_front(_coordActual);
			}
			_cajasADepositar = Anterior.cajasADepAnterior;
		}
		_coordActual = Anterior.coordAnterior;
	}
	_Movimientos.pop();
	
}

bool Sokoban::hayParedEnSoko(Coordenada p) const{
	return _mapa.hayPared(p);	
}

bool Sokoban::hayDepositoEnSoko(Coordenada d) const{
	return _mapa.hayDeposito(d);
}

bool Sokoban::Pertenece(const list<Coordenada>& l, Coordenada c) const{
    bool res = false;
    for(list<Coordenada>::const_iterator it = l.begin(); it != l.end(); ++it){
        if(*it == c){
            res = true;
        }
    }
    return res;
}

void Sokoban::EliminarCaja(list<Coordenada>& L, Coordenada c){
	bool termine=false;
	list<Coordenada>::iterator it1=L.begin();
	while(termine==false)
	{
		if ((*it1)==c){
				it1 = L.erase(it1);
				termine=true;
		}
		else{
			++it1;
		}
	}

}

